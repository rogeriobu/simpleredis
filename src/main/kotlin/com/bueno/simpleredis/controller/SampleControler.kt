package com.bueno.simpleredis.controller

import com.bueno.simpleredis.model.Person
import com.bueno.simpleredis.service.SampleService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(value = "/api/person/")
class SampleController(val sampleService: SampleService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addPerson(@RequestBody person: Person): ResponseEntity<Person> {
        return ResponseEntity.ok(sampleService.savePerson(person))
    }

    @GetMapping(value = ["/id/{id}"])
    fun findPerson(@PathVariable("id") id: Long): ResponseEntity<Person> {
        return ResponseEntity.ok(sampleService.getPerson(id))
    }

    @GetMapping
    fun findAllPerson(): ResponseEntity<List<Person>> {
        return ResponseEntity.ok(sampleService.getAllPerson())
    }
}
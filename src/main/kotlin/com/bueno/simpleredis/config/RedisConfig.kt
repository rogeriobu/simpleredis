package com.bueno.simpleredis.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.serializer.GenericToStringSerializer



@Configuration
class RedisConfig {

    @Bean
    fun redisConnectionFactory(): JedisConnectionFactory {
        return JedisConnectionFactory()
    }


    @Bean
    fun redisTemplate(): RedisTemplate<String, Any> {
        val template = RedisTemplate<String, Any>()
        template.setConnectionFactory(redisConnectionFactory())
        template.valueSerializer = GenericToStringSerializer(Any::class.java)
        return template
    }


}
package com.bueno.simpleredis.repository

import com.bueno.simpleredis.model.Person

interface PersonRepository {
    fun findAllPerson(): Map<Long, Person>
    fun addPerson(person: Person) : Person
    fun delete(id: Long)
    fun findPerson(id: Long): Person
}
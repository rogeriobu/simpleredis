package com.bueno.simpleredis.repository

import com.bueno.simpleredis.model.Person
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class PersonRepositoryImpl(private val redisTemplate: RedisTemplate<String, Any>) : PersonRepository {

    @Value("\${redis.counter.key:PERSON}")
    private lateinit var KEY: String
    private lateinit var hashOps: HashOperations<String, Long, Person>

    @PostConstruct
    private fun init() {
        hashOps = redisTemplate.opsForHash<Long, Person>()
    }

    override fun addPerson(person: Person) : Person{
        hashOps.put(KEY, person.id, person)
        return findPerson(person.id)
    }

    override fun delete(id: Long) {
        hashOps.delete(KEY, id)
    }

    override fun findPerson(id: Long): Person {
       return hashOps.get(KEY, id)!!
    }

    override fun findAllPerson(): Map<Long, Person> {
        return hashOps.entries(KEY)
    }
}
package com.bueno.simpleredis.service

import com.bueno.simpleredis.model.Person
import com.bueno.simpleredis.repository.PersonRepository
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Service

@Service
class SampleService(val personRepository: PersonRepository){

    fun savePerson(person: Person) = personRepository.addPerson(person)

    fun getPerson(personId: Long) : Person = personRepository.findPerson(personId)

    fun getAllPerson() : List<Person> = personRepository.findAllPerson().values.toList()

}

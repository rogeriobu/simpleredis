package com.bueno.simpleredis.model

import java.io.Serializable

data class Person(val id: Long,
                  val name: String) :Serializable {

}